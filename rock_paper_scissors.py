import random
import time
start_time = time.time()
#Available options for the game
available_choice = ('ROCK','PAPER','SCISSORS')

#Create a dictionary within a list to save each round choices
game_history =[0]
game_history_dict = {}

#Saves the winner of each round
game_record = {}
print("*PLEASE ENTER VALID CHOICE!*")

#Lists of corresponding result compared with computer choice, score also stored here 
user_rock =  [{1:'TIE',2:0},{1:'COMPUTER WINS!',2:0},{1:'PLAYER WINS',2:0}]
user_paper = [{1:'PLAYER WINS!',2:0},{1:'TIE',2:0},{1:'COMPUTER WINS!',2:0}]
user_scissors = [{1:'COMPUTER WINS!',2:0},{1:'PLAYER WINS!',2:0},{1:'TIE',2:0}
]
#for loop used for repeating rounds
for i in range (1,11):
    print('\n ROUND  '+ str(i))

    #User selects from the options provided
    user_choice = input("Select from ROCK ,PAPER , OR SCISSORS  :  ")

    #Computer randomly generates from available_choice
    computer_choice = random.choice(available_choice)
    print('The computer choice is '+computer_choice)

    #Assigns computers's choice with a number using dictionary
    choice_dict= {'ROCK':0,'PAPER':1,'SCISSORS':2}

    #saves player and computer choice into predeined list,dictionary
    game_history_dict ['PLAYER'] = user_choice
    game_history_dict ['COMPUTER'] = computer_choice
    computer_key = choice_dict[computer_choice]
    user_key = choice_dict[user_choice]

    #if-elif statement for user statements
    if user_key == 0 :
        game_result = user_rock[computer_key][1]
        user_rock[computer_key][2]+=1
    elif user_key == 1:
       game_result = user_paper[computer_key][1]
       user_paper[computer_key][2]+=1
    else:
        game_result = user_scissors[computer_key][1]
        user_scissors[computer_key][2]+=1
        
    print (game_result)

    #score added from each list
    u_score = user_rock[2][2]+user_paper[0][2]+user_scissors[1][2]
    c_score = user_rock[1][2]+user_paper[2][2]+user_scissors[0][2]
    game_history_dict ['WINNER'] = game_result
    game_history.append(game_history_dict.copy())

    print('PLAYER CHOSE : '+str(user_choice)+'\n'+'COMPUTER CHOSE : '+str(computer_choice))

#Final result score
print('\n GAME COMPLLETE! \n PLAYER POINTS:'+str(u_score)+'\n'+'COMPUTER POINTS:'+str(c_score)+'\n')
if u_score == c_score:
    print('THE GAME IS A TIE!')
elif u_score > c_score:
    print ('PLAYER WINS THE GAME!')
else:
    print ('COMPUTER WINS THE GAME!')

    #Input for game details
while(True):
    question = input('Do you want to know round details? y/n : ')
    if question == 'n':
        break
    game_detail = input("\n Enter round number: ")
    game_details= int(game_detail)
    print('GAME DETAILS: \n PLAYER CHOICE: '+str(game_history[game_details]['PLAYER'])+'\n COMPUTER CHOICE: '+str(game_history[game_details]['COMPUTER'])+'\n RESULT:'+str(game_history[game_details]['WINNER']))
print(str(time.time()-start_time))
